#!/bin/bash -u

# jhalfmoon
# Yabagi Prompt ; YA Boi's bAsh GIt Prompt.
#
# Yabagi displays git repository status in your bash prompt.
#
# - Named as to be easily found by Google.
# - No configuration required; Clone the repo, add a line to .bashrc and you're done.
# - Easy customization;  No complex config files; Just edit one well-commented and readable bash script.
# - Easy customization;  No complex config files; Just edit one well-commented and readable bash script.
#   Or put a yabagi.conf file in $HOME. An example config file is supplied.
# - Does periodic background 'git fetch' to sync with remote repo and find out the ahead / behind status of branches.
# - Can wait for the remote sync to complete, so that the ahead/behind status matches that of the remote repo. Off by default.
#   Set yabFetchWait to the amount of seconds that you wish to wait for a valid answer.
# - Works with ancient and modern git versions. A few git versions that yabagi was tested with:
#   - v2.30.0 - release date 27 December 2020, Arch Linux 2020.1
#   - v1.8.3.1 - release date June 9 2013, RHEL 7
#   - v1.7.1 - release date April 23 2010, Centos 6
# - Extra feature 1: Shows state of previous command in red cross or green tick, along with return code.
# - Extra feature 2: Set the title bar of the terminal window.
# - Can be used with 'set -x' active in your shell, without clobbering your terminal. See 'yabTraceMute'.
#
# USAGE: Add the following line to .bashrc :
#   source /path/to/script/yabagi-prompt.sh
#
# - On Windows, using Msys2 or Cygwin for example, git may be very slow and thus yabagi will be slow, to the point of being unusable.
#   There's a thread on that with a list of possible solutions on [stackoverflow:
#   https://stackoverflow.com/questions/42888024/git-bash-mintty-is-extremely-slow-on-windows-10-os
# - Yabagi will fetch all remotes during syncs, not just the remote of the current branch. This should not be an issue in most situations.
# - Bare repos will be detected and shown as \<bare\>, but yabagi will not sync them. Bare repos do not require yabagi's services.
# - Yabagi might fail silently if you have a misconfiguration in your .gitconfig. Using a very old git version with a new config file might trigger that.
# - All variables are either local or prefixed with 'yab', except for the colors; Those are pre- suffixed with underscores.
#   This is done to prevent possible interference with any of your other bash variables.
# - On color code usage in bash: Always place color codes after white spaces, otherwise the last character before the space might be partially overwritten.
# - On branches with unnamed remotes, git acts weird: It will not calculate ahead / behind status. Yabagi will show a red '?' on your prompt to flag
#   this situation. An unnamed remote is when a branch has a remote defined as an URL or path, instead of a remote name. They have no [remote] entry
#   in .git/config, only a 'remote=' entry under [branch $branchname].

# Set this variable to '0' to turn yabagi off. Good for debugging.
yabEnable=1
# Set this variable to '0' and do 'set -x' in you shell, to see what yabagi is actually doing. Good for debugging.
yabTraceMute=1
# If enabled, yabagi will insert a string into the prompt, that will set the title of your terminal window
yabSetWindowTitle=1
# When set, always tries to find the first tag of a commit, instead of just showing what git-branch spits out.
yabResolveTag=1
# Interval in minutes to check remote git repos for updates. Set to 0 to disable checking.
yabFetchInterval=15
# Interval in minutes to retry a fetch if it failed. Set to 0 to disable timeout.
yabFetchTimeout=2
# Maximum amount of seconds to wait for sync to complete. Set to 0 to not wait, ie. do a background fetch
yabFetchWait=0

# Define bash related bit of prompt
yabSetBasePrompt() {
    if [ "$(whoami)" == 'root' ] ; then
        BasePS1="${__red}\u@\h ${__blue}\w ${__red}\\$ ${_reset}"
    else
        BasePS1="${_green}\u@\h ${__blue}\w \\$ ${_reset}"
    fi
}

# Define order and formatting of the components of the prompt
# Tip: Inserting \n characters for multi-line prompts
yabSetFinalPrompt() {
    PS1="${_reset}${LastCommand} ${GitStatus}${BasePS1}"
}

yabStyle() {
    # The following variable defines the string that is used to set your terminal window title
    # In the following example, the $ENV variable, can be used by a password manager to determine which passwords to autotype
    yabWindowTitleCmd='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}   (ENV:${ENV:-default}) \007"'

    yabStyleCmdOk="\n${_green}✓"
    yabStyleCmdFail="\n${_red}✘"
    yabStyleVenvPrefix="${_blue}("
    yabStyleVenvSuffix="${_blue}) "

    yabStylePrefix="${__yellow}["
    yabStyleSuffix="${__yellow}] "
    yabStyleSeperator="${__yellow} "

    yabStyleBranch="${__yellow}"
    yabStyleAhead="${_green}↑"
    yabStyleBehind="${__red}↓"
    yabStyleNotSynced="${__red}!"
    yabStyleUnnamedRemote="${__red}?"

    yabStyleStaged="${__cyan}●"
    yabStyleModified="${__magenta}+"
    yabStyleUntracked="${__red}?"
    yabStyleStashed="${_green}≡"
    yabStyleConflicted="${_red}⊗"

    yabStyleBare="${_yellow}<bare>"
    yabStyleEmpty="${_yellow}<empty>"
    yabStyleDetached="${__cyan}D"
    yabStyleLocalBranch="${__yellow}L"
    yabStyleRemote=""
    # NOTE: More often than not, a branch will have a remote, so by default this is not shown in the prompt.
    #       Uncomment the following line if an explicit indication of having a remote is desired.
    # local yabStyleRemote="${green}R"
}

yabShow() {
    # Show all yabagi variables
    set | grep '^yab' | grep -v '()'
}

yabBuildPrompt() {
    local Temp=''
    local GitStatus=''

    # Test if this is a git repo and get some metadata if it is and only build a prompt if we are on a git repo
    ([ $yabEnable -eq 1 ] && which git &> /dev/null) && read -a Temp <<< $(git rev-parse --git-dir --is-bare-repository 2> /dev/null| tr "\n" " ")
    if [[ ${#Temp[@]} -eq 2 ]] ; then
        # Initialize the state tracking variables
        local Branch=''         # Note: This variable stores either the branch name or commit hash/tag. Same ambiguity as with git-checkout.
        local RemoteBranch=''
        local RemoteName=''
        local CommitHash=''
        local SyncStatus=''
        local RepoFlags=''
        local IsDetached=0

        local Stashed=0
        local Ahead=0
        local Behind=0
        local Staged=0
        local Modified=0
        local Conflicted=0
        local Untracked=0

        # Process results from the previous git rev-parse command
        local GitPath=${Temp[0]}
        # And use it to determine if repo is bare
        if [[ ${Temp[1]} = 'true' ]] ; then
            # Bare repos have no files checked out, so we can't do anything that yabagi was made for
            Branch="${yabStyleBare}"
        else
            # Get stash status
            local Stashed=$(git stash list 2> /dev/null | wc -l)
            # Determine what state the contents of the current commit is in (count files that are modified, untracked, staged etc.)
            local GitStatus=$( LC_ALL=C git status --untracked-files=normal --porcelain 2> /dev/null )
            [[ "$?" -ne 0 ]] && return 0
            while IFS='' read -r line || [[ -n "$line" ]]; do
                status=${line:0:2}
                case "$status" in
                    AA ) ((Conflicted++)) ;;
                    U? ) ((Conflicted++)) ;;
                    \?\?) ((Untracked++)) ;;
                    ?\ ) ((Staged++)) ;;
                    \ ?) ((Modified++)) ;;
                    ??) ((Modified++)) ; ((Staged++));;
                esac
            done <<< "$GitStatus"

            # Get branch/tag name and commit hash and when on branch, determine type (empty, local-only, non-local-only)
            Temp=$(git branch -vv --no-color | grep -e "^*" | tr -s ' ')
            # NOTE: git branch < v2.4.0 will say 'detached at' and >= 2.4.0 may say 'detached from' or 'detached at' (see git commit 4b06318664)
            if [[ "$Temp" =~ detached\ +(at|from) ]] ; then
                # Extract name (hash or tag) of commit
                Branch="$(echo "$Temp" | cut -d')' -f1 | rev | cut -d' ' -f1 | rev)"
                CommitHash="$(echo "$Temp" | cut -d')' -f2 | cut -d' ' -f2)"
                IsDetached=1
            # NOTE: 1) git branch < v1.8.3 will output "(no branch)" and only give a commit hash and no tag name.
            #       2) all git versions will out "(no branch," when rebasing/conflicted.
            elif [[ "$Temp" =~ \(no\ branch(\)|,) ]] ; then
                CommitHash="$(echo "$Temp" | cut -d')' -f2 | cut -d' ' -f2)"
                Branch=$CommitHash
                IsDetached=1
            else
                # HEAD is on a branch
                # Get branch name
                Branch=$(echo "$Temp" | cut -d' ' -f2)
                # NOTE: At this point, the remote name will be parsed out in two ways and the compared. This is to handle some weird git behavior
                # where 'git-branch -vv' only returns a string for the remote if a named remote exist. Unnamed remotes (URLs) are not shown.
                # Get remote path from git-branch command
                [[ "$Temp" =~ ' [' ]] && RemotePath=$(echo "$Temp" | cut -d'[' -f2 | cut -d']' -f1 | cut -d':' -f1 )
                # Parse out the name of the remote (ie. cut of the branch name from the remote path)
                Temp=$(echo $RemotePath | cut -d/ -f1)
                # Now the get the remote name from git-config. This will be compared with the above output to determine if this is an unnamed remote.
                RemoteName=$(git config branch.$Branch.remote)
                if [ -z "$Branch" ] ; then
                    # If no branch name is set, then this is an empty repo, with no commits
                    Branch="${yabStyleEmpty}"
                elif [ -z "$RemoteName" ] ; then
                    # If no remote exist for this branch, then this is a local-only branch
                    RepoFlags=${yabStyleLocalBranch}${RepoFlags}
                elif [ "$Temp" != "$RemoteName" ] ; then
                    # If the the names of the remote from git-config and git-branch are not the identical, then we are on an 'unnamed' remote.
                    # Git can or will not calculate ahead/behind on unnamed remotes, so we do not have to try and get those numbers.
                    SyncStatus=${yabStyleUnnamedRemote}
                else
                    RepoFlags=${yabStyleRemote}${RepoFlags}
                    SyncStatus=${yabStyleNotSynced}
                    # This branch has a remote, so check if we need to do a git-fetch
                    if [ $yabFetchInterval -gt 0 ] ; then
                        FetchHead=${GitPath}/FETCH_HEAD
                        if [ ! -e $FetchHead ] || [ -n "$(find $FetchHead -mmin +$yabFetchInterval 2> /dev/null)" ] ; then
                            # If $FetchHead is too old, then do a 'git fetch' to refresh ahead/behind data
                            # First remove $FetchHead, to prevent a race condition where yabagi thinks the fetch is done, but is hasn't even started
                            rm -f $FetchHead
                            ( exec &> /dev/null ; git fetch --all ${yabFetchJobs} --quiet & disown -h )
                            # If so requested: Wait for 'git fetch' to complete
                            if [ "$yabFetchWait" -gt 0 ] ; then
                                WaitCount=$yabFetchWait
                                while [ $WaitCount -gt 0 ] && ([ ! -s $FetchHead ] || [ -n "$(find $FetchHead -mmin +$yabFetchInterval)" ]) ; do
                                    ((WaitCount--))
                                    sleep 1
                                done
                            fi
                        elif [ ! -s "$FetchHead" ] && [ ${yabFetchTimeout} -gt 0 ] && [ -n "$(find $FetchHead -mmin +${yabFetchTimeout} 2> /dev/null)" ] ; then
                            # If $FetchHead is older than 2 minutes, but still has size zero, then assume something is preventing git-fetch from
                            # completing. Retry and do not wait for it to complete. NOTE: On slow connections or with very large repos, git fetch
                            # might take a long time and you might hit this time out. In that case you should adjust the time out value.
                            rm -f $FetchHead
                            ( exec &> /dev/null ; git fetch --all ${yabFetchJobs} --quiet & disown -h )
                        fi
                        if [ -s "$FetchHead" ] && [ -z "$(find $FetchHead -mmin +$yabFetchInterval 2> /dev/null)" ] ; then
                            # If $FetchHead is non-zero and younger then $yabFetchInterval, then clear SyncStatus, which means 'is synced'
                            SyncStatus=''
                        fi
                    fi
                    # The ahead/behind check must be done after the sync has been triggered, in case we want immediate results when yabFetchWait > 0
                    # NOTE1: git-rev-list supports --count from v1.7.2 on. Not used here for compatibility.
                    # NOTE2: git-status supports --ahead-behind from v2.16.2 on. Not used here for compatibility.
                    Temp=$(git rev-list --left-right HEAD...${RemotePath} --)
                    Ahead=$(echo "$Temp"  | grep -c '^<' )
                    Behind=$(echo "$Temp" | grep -c '^>' )
                fi
            fi
            # If the git-branch branch name is not equal to the hash, then assume it is a good tag and do nothing here.
            # If detached and the commit name in the git-status line is equal to the commit hash, then:
            # Replace commit hash with tag name, if available, because 'git branch' does not always return the tag.
            if [ $IsDetached -eq 1 ] ; then
                RepoFlags=${yabStyleDetached}${RepoFlags}
                if [ $yabResolveTag -eq 1 ] && [ "$Branch" == "$CommitHash" ] ; then
                    Temp=$(git describe --exact-match --tags 2> /dev/null) && Branch=$Temp
                    # Note: The following code does more or less the same, but is 2 times as slow. Kept for future reference.
                    # Temp=$(git tag --points-at HEAD | tail -n1) && Branch=$Temp
                fi
            fi
        fi

        local Part1=""
        [ "$Ahead"  -ne 0 ] && Part1="${yabStyleAhead}${Ahead}"
        [ "$Behind" -ne 0 ] && Part1="${Part1}${yabStyleBehind}${Behind}"
        [ -n "$RepoFlags" ] && Part1="${RepoFlags}${Part1}"
        [ -n "$Part1"     ] && Part1=" ${Part1}"

        Part1="${yabStylePrefix}${yabStyleBranch}${Branch}${SyncStatus}${Part1}"

        local Part2=""
        [ "$Staged"     -ne 0 ] && Part2="${yabStyleStaged}${Staged}"
        [ "$Modified"   -ne 0 ] && Part2="${Part2}${yabStyleModified}${Modified}"
        [ "$Untracked"  -ne 0 ] && Part2="${Part2}${yabStyleUntracked}${Untracked}"
        [ "$Conflicted" -ne 0 ] && Part2="${Part2}${yabStyleConflicted}${Conflicted}"
        [ "$Stashed"    -ne 0 ] && Part2="${Part2}${yabStyleStashed}${Stashed}"

        if [ -z "${Part2}" ] ;  then
            GitStatus="${Part1}${yabStyleSuffix}"
        else
            GitStatus="${Part1}${yabStyleSeperator}${Part2}${yabStyleSuffix}"
        fi
    fi

    if [ -n "${VIRTUAL_ENV}" ] ; then
        GitStatus="${yabStyleVenvPrefix}$(basename "${VIRTUAL_ENV}")${yabStyleVenvSuffix}${GitStatus}"
    fi

    if [ $LastCommandState -eq 0 ] ; then
        LastCommand="${yabStyleCmdOk}"
    else
        LastCommand="${yabStyleCmdFail} ${LastCommandState}"
    fi

    yabSetFinalPrompt

    # Restore the 'set -x' state, if it was set when yabGetLastCommandState was called
    [[ $yabShellState =~ 'x' ]] && set -x
}

yabGetLastCommandState() {
    LastCommandState=$?
    # Store the bash set options. See comment above the variable declaration for why this is done.
    yabShellState=$-
    [ $yabTraceMute -eq 1 ] && set +x
}

yabSetColorName() {
    eval $1=\''\[\033['$2'm\]'\'
}

# Usable color names:
#  ${_color}   normal color
#  ${__color}  bold / bright
#  ${___color} dim color
#  ${_reset}   reset style
yabGenerateColors() {
    _reset='\[\033[0m\]'
    names="black red green yellow blue magenta cyan white"
    index=30
    for name in $names; do
        yabSetColorName _${name}   "0;$index"
        yabSetColorName __${name}  "1;$index"
        yabSetColorName ___${name} "2;$index"
        ((index++))
    done
}

# The following (global) variable is used to store the 'set -x' state during yabagi runs
yabShellState=''

# Source config file if present
[ -r $HOME/yabagi.conf ] && source $HOME/yabagi.conf

if ! (which git &> /dev/null) ; then
    echo "WARNING: Yabagi Prompt is running but could not find git."
else
    # parallel fetches are supported in git >= v2.24.0
    yabGitVersion=$(git --version | cut -d ' ' -f3 | tr -d '.' | cut -b1-3)
    if [ $yabGitVersion -ge 224 ] ; then
        yabFetchJobs='-j3'
    else
        yabFetchJobs=''
    fi
fi

yabGenerateColors
yabStyle
yabSetBasePrompt

# Optionally add the window title string
if [ $yabSetWindowTitle -eq 1 ] ; then
   PROMPT_COMMAND="$PROMPT_COMMAND ; $yabWindowTitleCmd"
fi
# PROMPT_COMMAND is where yabagi gets triggered, which then rebuilds PS1
PROMPT_COMMAND="yabGetLastCommandState ; $PROMPT_COMMAND ; yabBuildPrompt"
# Remove any duplicate semicolons
PROMPT_COMMAND=$(echo "$PROMPT_COMMAND" | sed -E 's/ *;[ ;]+/;/g')

# EOF

