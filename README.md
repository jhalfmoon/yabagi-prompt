# YABAGI - YA Boi's bAsh GIt Prompt

... is swankier than thou. Yabagi displays git repository status in your bash prompt.

- Clean: One compact Bash script.
- Minimal dependencies: Bash, Git.
- Complete: Shows you all you need to know.

Use it and you'll ask yourself how you've ever managed without it. At the time that I started building this tool, there were similar projects out there. But I found them either overly complicated or lacking in features. So I used those tools as a starting point and built this tool. After having used and tweaked it for over four years now, it has become it's own type of beasty. This is what it can look like:

![Yabagi in all its glory](img/example-1.png)

Try it. You'll like it.

## Usage

- Clone the repo
- Add the following line to .bashrc :
```
    source /path/to/script/yabagi-prompt.sh
```
- To start using it, manually source the file once or just open a new terminal.

## About

Some notable features of this tool:

- Named as to be easily found by Google.
- No configuration required; Clone the repo, add a line to .bashrc and you're done.
- Easy customization;  No complex config files; Just edit one well-commented and readable bash script. Or put a yabagi.conf file in $HOME. An example config file is supplied.
- Does periodic background 'git fetch' to sync with remote repo and find out the ahead / behind status of branches.
- Can wait for the remote sync to complete, so that the ahead/behind status matches that of the remote repo. Off by default. Set yabFetchWait to the amount of seconds that you wish to wait for a valid answer.
- Works with ancient and modern git versions. A few git versions that yabagi was tested with:
  - v2.30.0 - release date 27 December 2020, Arch Linux 2020.1
  - v1.8.3.1 - release date June 9 2013, RHEL 7
  - v1.7.1 - release date April 23 2010, Centos 6
- Extra feature 1: Shows state of previous command in red cross or green tick, along with return code.
- Extra feature 2: Set the title bar of the terminal window.
- Can be used with 'set -x' active in your shell, without clobbering your terminal. See 'yabTraceMute'.

NOTE:

- On Windows, using Msys2 or Cygwin for example, git may be very slow and thus yabagi will be slow, to the point of being unusable. There's a thread on that with a list of possible solutions on [stackoverflow](https://stackoverflow.com/questions/42888024/git-bash-mintty-is-extremely-slow-on-windows-10-os).
- Yabagi will fetch all remotes during syncs, not just the remote of the current branch. This should not be an issue in most situations.
- Bare repos will be detected and shown as \<bare\>, but yabagi will not sync them. Bare repos do not require yabagi's services.
- Yabagi might fail silently if you have a misconfiguration in your .gitconfig. Using a very old git version with a new config file might trigger that.
- All variables are either local or prefixed with 'yab', except for the colors; Those are pre- suffixed with underscores. This is done to prevent possible interference with any of your other bash variables.
- On color code usage in bash: Always place color codes after white spaces, otherwise the last character before the space might be partially overwritten.
- On branches with unnamed remotes, git acts weird: It will not calculate ahead / behind status. Yabagi will show a red '?' on your prompt to flag this situation. An unnamed remote is when a branch has a remote defined as an URL or path, instead of a remote name. They have no [remote] entry in .git/config, only a 'remote=' entry under [branch $branchname]. In the following snippet, an example of branches with named remotes are 'master' and 'normalbranch'. The branch with the unnamed remote is 'weirdbranch'. Note that normalbranch and weirdbranch both use the same source URL, only defined differently.

```
[remote "origin"]
        url = https://github.com/someproject/someproject.git
        fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
        remote = origin
        merge = refs/heads/master
[remote "myremote"]
        url = git@gitlab.com:myusername/someproject-my-fork.git
        fetch = +refs/heads/*:refs/remotes/myremote/*
[branch "normalbranch"]
        remote = myremote
        merge = refs/heads/normalbranch
[branch "weirdbranch"]
        remote = git@gitlab.com:myusername/someproject-my-fork.git
        merge = refs/heads/weirdbranch
```

## Testing

A test script is supplied in the 'test' directory. Its purpose is to execute a semi-automatic test of yabagi-prompt with several git versions. It generates output that needs further manual inspection detect any weirdness. The tests require docker to be up and running. First run test/build-images.sh to build the docker images. Then run test/run-test.sh to test yabagi with the different git versions.

## Configuration

Although yabagi will work just fine, straight out of the box, you can tweak it if you want to. Yabagi's look and some behavior can be controlled by directly editing Yabagi's code or by placing a yabagi.conf file in your homedir. An external config file has the added benefit of keeping your customization in tact. All configurables are demonstrated in the supplied example config file.

The following table lists a few non style related configurables, that can be modified in your shell to control the behavior of yabagi.

| Variable | default    | Info    |
| --- | --- | --- |
| yabEnable | 1 | When false, yabagi will not process any git information on the prompt. This also disables background fetching. Useful for debugging. |
| yabTraceMute| 1 | When false and you do 'set -x' in you shell, you can see what yabagi is actually doing. By default yabagi will mute the output of itself, to prevent from clobbering your terminal. Useful for debugging. |
| yabResolveTag | 1 | When true, yabagi will always try to resolve commit hashes to tags. If a commit has multiple tags, it will use whatever tag git suggests, or if git suggests using a commit hash, then the first tag found for that commit will be shown. When false, yabagi will display exactly what 'git-branch -vv' shows, which varies depending on how and what you've checked out the current commit.
| yabSetWindowTitle| 1 | When true, yabagi will insert a string into the prompt, that will set the title of your terminal window. |
| yabFetchInterval | 15 | Interval in minutes to check remote git repos for updates. Set to 0 to disable checking. |
| yabFetchTimeout | 2 | Interval in minutes to retry a fetch if it failed. Set to 0 to disable timeout. |
| yabFetchWait | 0 | Maximum amount of seconds to wait for sync to complete. Set to 0 to not wait, ie. do a background fetch. |

## Examples

Prompt | Explanation
--- | ---
✓ [master!] /tmp/myrepo $ | On git repo, master branch, ahead/behind status not yet known, no changes
✓ [master ↑1↓2] /tmp/myrepo $ | On git repo, master branch, Ahead by 1 commit, Behind by 2 commits, no changes
✓ [master ●2+1?3] /tmp/myrepo $ | On master branch, two staged files, one modified unstaged file, three untracked files
✓ [master L ⊗1] /tmp/myrepo-local-only $ | On a local-only repo, master branch,  one pending conflict
✓ [2364a201a3 D ≡1] /tmp/myrepo $ | On detached HEAD, One session stashed
✓ [imadolphin D] /tmp/myrepo $ | On detached HEAD,  on tag 'imadolphin'
✓ [\<bare\>] /tmp/myrepo-bare $ | On a bare repo
✓ [\<empty\>] /tmp/myrepo $ | On an empty local repo
✓ /var/log $ | Not on a git repo, Exit status good/0
✘ 128 /var/log $ | Not on a git repo, Exit status bad/128

## Local Status Symbols

Symbol | Explanation
--- | ---
``●n``| ``n`` staged files
``+n``| ``n`` changed but unstaged files
``?n``| ``n`` untracked files
``≡n``| ``n`` stash entries
``⊗n``| ``n`` files with merge conflicts

## Branch Tracking Symbols

Symbol | Explanation
--- | ---
``↑n``| ahead of remote by ``n`` commits
``↓n``| behind remote by ``n`` commits
``D`` | on detached HEAD
``L`` | local branch or repo, no remote configured
``!`` | ahead / behind status is unknown and a background 'git fetch' has been started to get that information
``?`` | ahead / behind status is unknown, and will not be known because unnamed remote was detected, which git does not really support
``<empty>`` | Empty repository, with no commits
``<bare>`` | Bare repository

## Credits

This project was inspired by, and borrowed some code from, the following projects:

[zsh-git-prompt](https://github.com/olivierverdier/zsh-git-prompt)

[bash-git-prompt](https://github.com/magicmonty/bash-git-prompt)

[git-aware-prompt](https://github.com/jimeh/git-aware-prompt)
