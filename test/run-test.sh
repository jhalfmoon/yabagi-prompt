#!/bin/bash

# Run yabagi-test.sh with several different git versions

HERE=$(dirname $(readlink -f $0))
DOCKER="docker run -it --rm --mount type=bind,source="$HERE/..",target=/yabagi --env FORCE_GITCONFIG=1"

echo "Centos 6 ======================================"
$DOCKER local:yabagi-c6 /yabagi/test/yabagi-test.sh
echo "Centos 7 ======================================"
$DOCKER local:yabagi-c7 /yabagi/test/yabagi-test.sh
echo "Centos 8 ======================================"
$DOCKER local:yabagi-c8 /yabagi/test/yabagi-test.sh
echo "Centos 8 - Git from source ===================="
$DOCKER local:yabagi-c8 /yabagi/test/yabagi-test.sh /usr/local/mine/git/active/bin

