#!/bin/bash

# Build centos images that can be used to test yabagi-prompt with

HERE=$(dirname $(readlink -f $0))
DOCKER="docker build"

$DOCKER -t local:yabagi-c6 -f $HERE/dockerfiles/c6 $HERE/..
$DOCKER -t local:yabagi-c7 -f $HERE/dockerfiles/c7 $HERE/..
$DOCKER -t local:yabagi-c8 -f $HERE/dockerfiles/c8 $HERE/..

