#!/bin/bash

# 20201230 jhalfmoon
#
# Generate git repo scenarios to test yabagi with and run an expect script to generate yabagi output.
#
# This script can only test yabagi if yabagi is sourced in your bashrc, because 'expect' will launch
# a new shell, which it will control, and yabagi needs to be running in it, or it will not be tested.
#
# NOTE1: Testing for activation of yabagi inside a non-login bash shell, like this script is running in,
# for example, is not possible. Yabagi does not export any variables and thus they will not be visible
# outside the context of the login shell.
#
# NOTE2: Setting FORCE_GITCONFIG will forcefully set a few git config variable. If not set then it will be asked first.
#
# NOTE3: A single parameter to an alternative PATH can be given on the command line. Useful for testing againt self-compiled git versions.

BASEDIR=/tmp
[ -n "$1" ] && export PATH=$1:$PATH

fail() {
    echo "# ERROR: $1"
    exit 1
}

# Use date's second and nanosecond time to create unique content for files
gen() {
    date +%s:%N > $1
}

InitRepos() {
    local WORKDIR=$1

    if ! (mkdir -p $WORKDIR) ; then
        fail "Unable to create working directory '$WORKDIR'"
    fi
    pushd .

    #==================================
    # create the main repo with...
    #    three commits
    #    two tags on a single commit
    #    a branch
    MAIN=$WORKDIR/main
    mkdir $MAIN
    cd $MAIN
    git init
    gen file1 ; git add file1 ; git commit -m one
    gen file1 ; git add file1 ; git commit -m two
    git tag tag_1
    git tag tag_2
    git checkout -b mybranch1
    git checkout master

    #==================================
    # clone the main repo into bare repo
    MAIN_CLONE_BARE=$WORKDIR/main-clone-bare.git
    git clone --bare $MAIN $MAIN_CLONE_BARE

    #==================================
    # clone the main repo
    MAIN_CLONE=$WORKDIR/main-clone
    git clone $MAIN $MAIN_CLONE

    #==================================
    # on main: setup a branch which will create a conflict when merged
    cd $MAIN
    git checkout mybranch1
    git checkout -b mybranch2
    gen file4 ; git add file4 ; git commit -m three

    # make branch on remote repo one commit ahead of the clone
    git checkout mybranch1
    gen file4 ; git add file4 ; git commit -m four

    #==================================
    # add a branch with an unnamed remote to the main repo
    cd $MAIN_CLONE
    git checkout -b unnamed-remote
    git push --set-upstream $MAIN_CLONE_BARE unnamed-remote

    # make branch on clone one commit ahead of remote
    git checkout mybranch1
    gen file4 ; git add file4 ; git commit -m five

    popd
}

# NOTE: The histappend and history options are set/cleared to prevent expect from
#       cluttering up your .bash_history.
RunTest() {
    expect - $1 <<'EOF'
set DIR [lindex $argv 0]
set ID " \$ "
set timeout -1
spawn "bash"

expect $ID
send "shopt -u histappend\r"
expect $ID
send "set +o history\r"
expect $ID
send "\[ \$yabEnable -eq 1 \] || echo ERROR Yabagi is not active.\r"
expect $ID
send "echo PS1=\$PS1\r"
expect $ID
send "echo PROMPT_COMMAND=\$PROMPT_COMMAND\r"
expect $ID
send "unset VIRTUAL_ENV\r"
expect $ID

# test empty repo
send "mkdir $DIR/empty ; cd $DIR/empty ; git init\r"
expect $ID
# test empty-bare repo
send "mkdir $DIR/empty-bare ; cd $DIR/empty-bare ; git init --bare\r"
expect $ID
# test non-empty-bare repo
send "cd $DIR/main-clone-bare.git\r"
expect $ID

# test local-only branch
send "cd $DIR/main\r"
expect $ID
# test mode: conflicted merge
send "git merge mybranch2\r"
expect $ID
# test mode: new
send "echo 99 > file9\r"
expect $ID
# test mode: modified
send "echo 11 > file1\r"
expect $ID
# test mode: staged
send "echo 22 > file2 ; git add file2\r"
expect $ID

# test repo with a remote (will be unsynced at first)
send "cd $DIR/main-clone\r"
expect $ID
# the second time it should be synced
send "sleep 1\r"
expect $ID
# test tag resolution
send "git checkout HEAD~\r"
expect $ID
send "yabResolveTag=0\r"
expect $ID
send "yabResolveTag=1\r"
expect $ID
# test non-tagged (hash only) commind
send "git checkout HEAD~\r"
expect $ID
send "git checkout master\r"
expect $ID
# test if tag displays checked out tag name, even thoug the commit has 2 tags
send "git checkout tag_2\r"
expect $ID
send "git checkout -\r"
expect $ID
# test if tag displays checked out tag name, even thoug the commit has 2 tags
send "git checkout tag_1\r"
expect $ID

# test conflicted stash pop
send "git checkout master\r"
expect $ID
send "echo 1 > 1 ; git add 1 ; git stash ; echo 2 > 1 ; git add 1 ; git stash pop\r"
expect $ID
send "git checkout --ours 1\r"
expect $ID
send "git add 1\r"
expect $ID

# test branch whith unnamed remote
send "git checkout unnamed-remote\r"
expect $ID
# test venv
send "VIRTUAL_ENV=venv\r"
expect $ID
# test fail exit code
send "(exit 123)\r"
expect $ID
# test success exit code
send "(exit 0)\r"

expect $ID
send "exit &> /dev/null\r"
expect $ID

wait
EOF
}

git_check_var() {
    NAME=$1
    VALUE=$2
    if [ "$FORCE_GITCONFIG" -ne 1 ] && [ -e $HOME/.gitconfig ] && [ -n $(git config --get $NAME) ] ; then
        read -p "Git configaration $1 not found; Set it now? (y)/n " ANS
        if [ -n $ANS ] && [[ ! "$ANS" =~ y|yes|Y|YES ]] ; then
            return
        fi
    fi
    git config --global "$NAME" "$VALUE"
}

echo "# Starting yabagi test script"
if ! (which git &> /dev/null) ; then
    fail "git was not found. Please install it."
elif ! (which expect &> /dev/null) ; then
    fail "The application 'expect' was not found. Please install it."
else
    echo "# Using $(git version)"
    BASEDIR=${BASEDIR}/yabagi-test
    rm -rf ${BASEDIR} &> /dev/null

    # Setting the following configuration items makes the output less noisey
    git_check_var user.name           'Default User'
    git_check_var user.email          default@local.localdomain
    git_check_var advice.detachedHead false
    git_check_var init.defaultBranch  master

    echo "# Initializing test repos at ${BASEDIR}"
    InitRepos $BASEDIR/1 &> /dev/null
    echo "# Starting expect script; If test hangs here, you probably need to add 'source yabagi' to your .bashrc."
    RunTest $BASEDIR/1
    echo "#"
    echo "# Test repos are at ${BASEDIR}"
fi

#=== EOF

