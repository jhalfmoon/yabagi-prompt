#!/bin/bash

TAG=$1
DIR=/usr/local/mine/git/$TAG
mkdir -p $DIR
git clone https://github.com/git/git.git
cd git
git checkout $TAG
make configure
./configure --prefix=$DIR
make -j4
make install
ln -s $DIR /usr/local/mine/git/active

